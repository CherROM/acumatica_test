﻿using acumatica_test.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FirebirdSql.Data.FirebirdClient;
using System.Threading;
using System.Threading.Tasks;

namespace acumatica_test.Handlers
{
    internal sealed class StorageHandler
    {
        // =============== Singleton =============================================

        private static volatile StorageHandler _instance;
        private static readonly object _lockObj = new object();

        public static StorageHandler Instance
        {
            get
            {
                lock (_lockObj)
                {
                    if (_instance == null)
                        _instance = new StorageHandler();
                }

                return _instance;
            }
        }

        // =======================================================================

        static StorageHandler() { }

        private StorageHandler() { }

        private const string CONNECTION_STRING =
            @"User=SYSDBA;Password=1234;Database=D:\ACUMATICA_TEST.fdb;DataSource=localhost;Port=3050;Dialect=3;Charset=NONE;Role=;Connectionlifetime=15;Pooling=true;MinPoolSize=0;MaxPoolSize=50;Packet Size=8192;ServerType=0;";

        private const string QUERY_STRING =
            @"SELECT r.BUILD " +
            @"FROM BUILDNUM r " +
            @"WHERE r.YEARNUM = {0} AND r.REVISION = {1}";

        private const string INSERT_STRING =
            @"UPDATE OR INSERT INTO BUILDNUM (YEARNUM, REVISION, BUILD) " +
            @"VALUES ({0}, {1}, {2}) " +
            @"MATCHING (YEARNUM, REVISION)";

        private const string MUTEX_NAME = @"Global\DB_SYNC";

        public void StoreBuild(IEnumerable<BuildNum> buildNums)
        {
            var mutex = new Mutex(false, MUTEX_NAME);
            try
            {
                mutex.WaitOne();

                using (FbConnection connection = new FbConnection(CONNECTION_STRING))
                {
                    connection.Open();
                    var trans = connection.BeginTransaction();

                    try
                    {
                        foreach (var buildNum in buildNums)
                        {
                            var insert = new FbCommand(GetInsertString(buildNum), connection, trans);
                            insert.ExecuteNonQuery();
                        }

                        trans.Commit();
                    }
                    catch
                    {
                        trans.Rollback();
                        throw;
                    }
                }
            }
            finally
            {
                mutex.ReleaseMutex();
            }
        }

        public BuildNum RetrieveBuild(VersionNum versionNum)
        {
            using (FbConnection connection = new FbConnection(CONNECTION_STRING))
            {
                connection.Open();
                var query = new FbCommand(GetQueryString(versionNum), connection);
                var value = query.ExecuteScalar();

                return new BuildNum(versionNum, GetBuildnumber(value));
            }
        }

        private string GetQueryString(VersionNum versionNum)
        {
            return String.Format(QUERY_STRING, versionNum.Year.ToString(), versionNum.Revision.ToString());
        }

        private string GetInsertString(BuildNum buildNum)
        {
            return String.Format(INSERT_STRING, buildNum.Year.ToString(), buildNum.Revision.ToString(), buildNum.Build.ToString());
        }
        private Int16 GetBuildnumber(object value)
        {
            if (value is Int16) return (Int16)value;
            else return -1;
        }
    }
}