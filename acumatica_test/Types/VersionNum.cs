﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;

namespace acumatica_test.Types
{
    public class VersionNum
    {
        private const string INPUT_REGEX = @"^[0-9]{2}([0-9]{2})r([0-9]{3})$";

        public Byte Year { get; private set; }
        public Int16 Revision { get; private set; }

        /// <exception cref="System.ArgumentNullException">Thrown when the input is null</exception>
        /// <exception cref="System.FormatException">Thrown when the input doesn't match the defined format, i.e. "[0-9]{4}r[0-9]{3}"</exception>
        public static VersionNum Parse(String versionStr)
        {
            if (versionStr == null) throw new ArgumentNullException();

            var match = new Regex(INPUT_REGEX).Match(versionStr);

            if (!match.Success) throw new FormatException();

            return new VersionNum(Byte.Parse(match.Groups[1].Value), Int16.Parse(match.Groups[2].Value));
        }

        public static Boolean TryParse(String versionStr, out VersionNum versionNum)
        {
            versionNum = null;

            if (versionStr == null) return false;

            var match = new Regex(INPUT_REGEX).Match(versionStr);

            if (!match.Success) return false;

            versionNum = new VersionNum(Byte.Parse(match.Groups[1].Value), Int16.Parse(match.Groups[2].Value));

            return true;
        }

        private VersionNum(Byte year, Int16 revision)
        {
            Year = year;
            Revision = revision;
        }

        protected VersionNum(VersionNum other) : this(other.Year, other.Revision) { }
    }
}