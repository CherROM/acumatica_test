﻿using acumatica_test.Types;
using acumatica_test.Types.Comparers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Threading;
using System.Timers;
using System.Web;
using System.Web.Caching;

namespace acumatica_test.Handlers
{
    internal sealed class CacheHandler
    {
        private const string CACHE_VAR_NAME = "VersionStorage";
        private const string MUTEX_NAME = @"Global\CACHE_SYNC";
        private const int STORAGE_UPDATE_THRESHOLD = 500;

        // =============== Singleton =============================================

        private static volatile CacheHandler _instance;
        private static readonly object _lockObj = new object();

        public static CacheHandler Instance
        {
            get
            {
                lock (_lockObj)
                {
                    if (_instance == null)
                        _instance = new CacheHandler();
                }

                return _instance;
            }
        }

        // =======================================================================

        static CacheHandler() { }

        private CacheHandler()
        {
            _cache = MemoryCache.Default;
            _storage = StorageHandler.Instance;
            _storageTimer = new System.Timers.Timer(STORAGE_UPDATE_THRESHOLD);
            _storageTimer.AutoReset = false;
            _storageTimer.Elapsed += storeCache;
        }

        private ObjectCache _cache;
        private StorageHandler _storage;
        private System.Timers.Timer _storageTimer;

        public BuildNum GetAndStoreNextBuildNum(VersionNum versionNum)
        {
            var mutex = new Mutex(false, MUTEX_NAME);
            try
            {
                mutex.WaitOne();

                _storageTimer.Enabled = false;

                if (_cache[CACHE_VAR_NAME] == null) reloadCache();
                var buildCache = _cache[CACHE_VAR_NAME] as Dictionary<VersionNum, Int16>;
                Int16 currentBuild;

                if (buildCache.ContainsKey(versionNum))
                {
                    currentBuild = buildCache[versionNum];
                }
                else currentBuild = retrieveStoredBuild(versionNum);

                buildCache[versionNum] = ++currentBuild;
                
                _storageTimer.Enabled = true;

                return new BuildNum(versionNum, currentBuild);
            }
            finally
            {
                mutex.ReleaseMutex();
            }
        }

        private void storeCache(Object source, ElapsedEventArgs e)
        {
            var buildCache = new Dictionary<VersionNum, Int16>(_cache[CACHE_VAR_NAME] as Dictionary<VersionNum, Int16>);
            _storage.StoreBuild(buildCache.Select(x => new BuildNum(x.Key, x.Value)).ToList());
        }

        private short retrieveStoredBuild(VersionNum versionNum)
        {
            return _storage.RetrieveBuild(versionNum).Build;
        }

        private void reloadCache()
        {
            _cache[CACHE_VAR_NAME] = new Dictionary<VersionNum, Int16>(new VersionNumComparer());
        }
    }
}