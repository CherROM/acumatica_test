﻿using acumatica_test.Handlers;
using acumatica_test.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace acumatica_test.Controllers
{
    public class VersionControlController : Controller
    {
        private const string FORMAT_ERROR_MSG = "Incorrect input format";

        [HttpGet]
        public ActionResult Index(string id)
        {
            if(!VersionNum.TryParse(id, out var versionNum))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, FORMAT_ERROR_MSG);

            var result = CacheHandler.Instance.GetAndStoreNextBuildNum(versionNum);

            return Content(result.ToString());
        }
    }
}