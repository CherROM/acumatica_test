﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace acumatica_test.Types.Comparers
{
    public class VersionNumComparer : IEqualityComparer<VersionNum>
    {
        public bool Equals(VersionNum x, VersionNum y)
        {
            return x.Year == y.Year && x.Revision == y.Revision;
        }

        public int GetHashCode(VersionNum obj)
        {
            var temp = obj.Year << 16 ^ obj.Revision;
            return temp;
        }
    }
}