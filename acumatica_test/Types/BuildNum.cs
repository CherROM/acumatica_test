﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace acumatica_test.Types
{
    public class BuildNum : VersionNum
    {
        private const Byte VERSION_STR_LEN = 11;

        private const String YEAR_FMT = "00";
        private const String REV_FMT = "000";
        private const String BUILD_FMT = "0000";

        public Int16 Build { get; private set; }

        internal BuildNum(VersionNum versionNum, Int16 build) : base(versionNum)
        {
            Build = build;
        }

        public override string ToString()
        {
            var sb = new StringBuilder(Year.ToString(YEAR_FMT), VERSION_STR_LEN);

            sb.Append(".");
            sb.Append(Revision.ToString(REV_FMT));
            sb.Append(".");
            sb.Append(Build.ToString(BUILD_FMT));

            return sb.ToString();
        }
    }
}