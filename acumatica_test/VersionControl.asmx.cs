﻿using acumatica_test.Handlers;
using acumatica_test.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace acumatica_test
{
    /// <summary>
    /// Summary description for VersionControl
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class VersionControl : System.Web.Services.WebService
    {

        [WebMethod]
        public string GetBuildNumber(string id)
        {
            if (!VersionNum.TryParse(id, out var versionNum))
                return null;

            var result = CacheHandler.Instance.GetAndStoreNextBuildNum(versionNum);

            return result.ToString();
        }
    }
}
